'use strict';

require('dotenv').config();
const fs = require('fs');
const keyby = require('lodash.keyby');
const shortid = require('shortid');
const logger = require('./log/panoEdges.log.js');

logger.info(`Migrating panoEdges to database ${process.env.DB_URL}`);

const compactObject = require('./compactObject');
const nodeModel = require('../../models/nodes');
const panoEdgeModel = require('../../models/panoEdges');

const RAW_DATA_PATH = `${__dirname}/raw`;
const floorsData = require(`${RAW_DATA_PATH}/floors.js`); //eslint-disable-line

async function migrateFloor(floor) {
  const floorId = floor.replace('LKS', 'LSK');
  const { startX = 0, startY = 0, migrateScale = 1 } = floorsData[floor];
  const startOffset = [startX, startY];

  const panoEdgeTextContent = fs.readFileSync(`${RAW_DATA_PATH}/map_data/pano_edges_${floor}.xml`, {
    encoding: 'utf8',
  });

  const nodeTextContent = fs.readFileSync(
    `${RAW_DATA_PATH}/map_data/intermediate_vertexes_${floor}.xml`,
    {
      encoding: 'utf8',
    },
  );

  const nodes = nodeTextContent
    .split('\n')
    .filter(line => Boolean(line.trim()) && !/^;*$/.test(line))
    .map(line => {
      const [id, coordinates] = line.split(';');
      const obj = compactObject({
        coordinates: coordinates
          .split(',')
          .map((v, i) => Math.round(parseInt(v, 10) * migrateScale) + startOffset[i]),
        id,
      });
      return obj;
    });

  const nodeById = keyby(nodes, o => o.id);

  const panoEdges = panoEdgeTextContent
    .split('\n')
    .filter(line => Boolean(line.trim()) && !/^;*$/.test(line))
    .map(line => {
      const [name, fromNodeName, toNodeName] = line.split(';');
      if (name === 'null') {
        return null;
      }
      return {
        _id: shortid(),
        name,
        fromNodeName,
        toNodeName,
        fromNodeCoordinates: nodeById[fromNodeName].coordinates,
        toNodeCoordinates: nodeById[toNodeName].coordinates,
        floorId,
      };
    })
    .filter(v => v);

  const count = { success: 0, fail: 0, skip: 0 };
  for (const edge of panoEdges) {
    const {
      fromNodeCoordinates,
      toNodeCoordinates,
      name,
      fromNodeName,
      toNodeName,
      ...others
    } = edge;

    const fromNodeSearchResults = await (await nodeModel.find({
      coordinates: fromNodeCoordinates,
      floorId,
    })).toArray();

    const toNodeSearchResults = await (await nodeModel.find({
      coordinates: toNodeCoordinates,
      floorId,
    })).toArray();

    const noFromNode = fromNodeSearchResults.length === 0;
    const noToNode = toNodeSearchResults.length === 0;
    const manyFromNode = fromNodeSearchResults.length > 1;
    const manyToNode = toNodeSearchResults.length > 1;
    let errorString = '[Skip] ';
    const errorDetail = { panoEdgeName: name };

    if (noFromNode || manyFromNode) {
      errorString += noFromNode ? '[no fromNode] ' : '[multiple fromNode] ';
      errorDetail.fromNodeName = fromNodeName;
      errorDetail.fromNodeCoordinates = fromNodeCoordinates;
    }

    if (noToNode || manyToNode) {
      errorString += noToNode ? '[no toNode] ' : '[multiple toNode] ';
      errorDetail.toNodeName = toNodeName;
      errorDetail.toNodeCoordinates = toNodeCoordinates;
    }

    if (noFromNode || manyFromNode || noToNode || manyToNode) {
      logger.warn(`${errorString}found of panoEdge ${name} on floor ${floorId}`, errorDetail);
      count.skip += 1;
      continue;
    }

    try {
      await panoEdgeModel.insertOne({
        ...others,
        fromNodeId: fromNodeSearchResults[0]._id,
        toNodeId: toNodeSearchResults[0]._id,
      });
      logger.verbose(`Inserted panoEdge ${name} on floor ${floorId} with _id ${others._id}`, {
        panoEdgeId: others._id,
        panoEdgeName: name,
        floorId,
        fromNodeId: fromNodeSearchResults[0]._id,
        fromNodeCoordinates,
        toNodeId: fromNodeSearchResults[0]._id,
        toNodeCoordinates,
      });
      count.success += 1;
    } catch (err) {
      logger.error(`[Error] MongoDB error on inserting panoEdge ${name} on floor ${floorId}`);
      count.fail += 1;
    }
  }

  logger.info(
    `Finish inserting panoEdges on floor ${floorId} (${count.success} success, ${count.fail} fail, ${count.skip} skip, ${panoEdges.length} total)`,
  );
}

async function migrate() {
  const floorIds = Object.keys(floorsData);

  // eslint-disable-next-line no-restricted-syntax
  for (const floorId of floorIds) {
    logger.info(`Migrating panoEdges on floor ${floorId}`);
    try {
      // eslint-disable-next-line no-await-in-loop
      await migrateFloor(floorId);
    } catch (err) {
      logger.error(`[Error] Uncaught error while migrating floor ${floorId}`, {
        error: err,
      });
    }
    logger.info(`Migrated panoEdges on floor ${floorId}`);
  }

  logger.info('Done');
  process.exit();
}

migrate();
