'use strict';

require('dotenv').config();
const fs = require('fs');
const logger = require('./log/nodesAndPanoImages.log.js');

logger.info(`Migrating Nodes and Pano Images to database ${process.env.DB_URL}`);

const compactObject = require('./compactObject');
const savePanoImage = require('../../routes/panoImages/savePanoImage');
const nodeModel = require('../../models/nodes');

const RAW_DATA_PATH = `${__dirname}/raw`;

// eslint-disable-next-line
const floorsData = require(`${RAW_DATA_PATH}/floors.js`);
const panoImageFolder = `${RAW_DATA_PATH}/map_panorama`;

async function migrateFloor(floor) {
  const nodeTextContent = fs.readFileSync(
    `${RAW_DATA_PATH}/map_data/intermediate_vertexes_${floor}.xml`,
    {
      encoding: 'utf8',
    },
  );

  const panoInfoTextContent = fs.readFileSync(`${RAW_DATA_PATH}/map_data/pano_info_${floor}.xml`, {
    encoding: 'utf8',
  });

  const floorId = floor.replace('LKS', 'LSK');
  const { startX = 0, startY = 0, migrateScale = 1 } = floorsData[floor];
  const startOffset = [startX, startY];

  const nodes = nodeTextContent
    .split('\n')
    .filter(line => Boolean(line.trim()) && !/^;*$/.test(line))
    .map(line => {
      const [id, coordinates, , panoImageName] = line.split(';');
      const obj = compactObject({
        coordinates: coordinates
          .split(',')
          .map((v, i) => Math.round(parseInt(v, 10) * migrateScale) + startOffset[i]),
        floorId,
        panoImageName: panoImageName.trim(),
        id,
      });
      return obj;
    })
    .filter(v => !!v.panoImageName);

  const usedPanoImages = new Set(nodes.map(v => v.panoImageName));

  const panoImages = panoInfoTextContent
    .split('\n')
    .filter(line => Boolean(line.trim()) && !/^;*$/.test(line))
    .map(line => {
      const [panoImageFile, westX] = line.split(';');
      const panoImageName = panoImageFile.trim().replace('.jpg', '');
      const panoImagePath = `${panoImageFolder}/${floor}/${panoImageFile.trim()}`;
      // corrseponding image not found in nodes. This is a unused image entry.
      if (!usedPanoImages.has(panoImageName)) {
        return null;
      }
      // corrseponding image used by a node but not found in fs. Remove this image.
      if (!fs.existsSync(panoImagePath)) {
        usedPanoImages.delete(panoImageName);
        return null;
      }
      const obj = compactObject({
        westX: parseInt(westX, 10),
        name: panoImageName,
        lastUpdatedAt: fs.statSync(panoImagePath).mtime.getTime(),
        data: fs.readFileSync(panoImagePath),
      });
      return obj;
    })
    .filter(v => !!v);

  const panoImageIdByName = {};

  let count = { success: 0, fail: 0 };
  for (const panoImage of panoImages) {
    try {
      // eslint-disable-next-line no-await-in-loop
      panoImageIdByName[panoImage.name] = (await savePanoImage(panoImage, true))._id;

      logger.verbose(`Inserted panoImage ${panoImage.name} on ${floorId}`, {
        panoImageName: panoImage.name,
        panoImageId: panoImageIdByName[panoImage.name],
      });
      count.success += 1;
    } catch (err) {
      logger.error(`[Error] MongoDB error on inserting panoImage ${panoImage.name} on ${floorId}`, {
        panoImageName: panoImage.name,
        error: err.message,
      });
      count.fail += 1;
    }
  }

  logger.info(
    `Finish inserting panoImages on floor ${floorId} (${count.success} success, ${count.fail} fail, ${panoImages.length} total)`,
  );

  count = { success: 0, fail: 0, skip: 0 };
  for (const node of nodes) {
    const oid = panoImageIdByName[node.panoImageName];

    const remoteNodes = await (await nodeModel.find({
      coordinates: node.coordinates,
      floorId,
    })).toArray();

    if (remoteNodes.length === 0) {
      logger.warn(`[Skip] No node found at specified coordinates ${node.coordinates}`, {
        nodeName: node.id,
        nodeCoordinates: node.coordinates,
        floorId,
        panoImageName: node.panoImageName,
        panoImageId: oid,
      });
      count.skip += 1;
      continue;
    } else if (remoteNodes.length > 1) {
      logger.warn(`[Skip] More than one nodes found at specified coordinates ${node.coordinates}`, {
        nodeName: node.id,
        nodeCoordinates: node.coordinates,
        floorId,
        panoImageName: node.panoImageName,
        panoImageId: oid,
      });
      count.skip += 1;
      continue;
    } else {
      try {
        await nodeModel.updateOne({ _id: remoteNodes[0]._id }, { panoImage: oid });
        logger.verbose('Updated node with panoImage', {
          nodeId: remoteNodes[0]._id,
          nodeCoordinates: node.coordinates,
          floorId,
          panoImageName: node.panoImageName,
          panoImageId: oid,
        });
        count.success += 1;
      } catch (err) {
        logger.error('[Error] MongoDB error on updating node with panoImage', {
          nodeId: remoteNodes[0]._id,
          nodeCoordinates: node.coordinates,
          floorId,
          panoImageName: node.panoImageName,
          panoImageId: oid,
          error: err,
        });
        count.fail += 1;
      }
    }
  }

  logger.info(
    `Finish updating nodes on floor ${floorId} (${count.success} success, ${count.fail} fail, ${count.skip} skip, ${panoImages.length} total)`,
  );
}

async function migrate() {
  const floorIds = Object.keys(floorsData);

  // eslint-disable-next-line no-restricted-syntax
  for (const floorId of floorIds) {
    logger.info(`Migrating nodes and panoImages on floor ${floorId}`);
    try {
      // eslint-disable-next-line no-await-in-loop
      await migrateFloor(floorId);
    } catch (err) {
      logger.error(`[Error] Uncaught error while migrating floor ${floorId}`, {
        error: err,
      });
    }
    logger.info(`Migrated nodes and panoImages on floor ${floorId}`);
  }

  logger.info('Done');
  process.exit();
}

migrate();
