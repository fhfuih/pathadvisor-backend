'use strict';

/* eslint-disable no-console */

const nodeModel = require('../../../models/nodes');
const panoImageModel = require('../../../models/panoImages');

/**
 * Manual update a node's panoImage reference
 * @param {Object} nodeQuery MongoDB query object to identify the node to update
 * @param {Object} panoImageQuery MongoDB query object to identify the panoImage to update
 */
async function updateNodePanoImage(nodeQuery, panoImageQuery) {
  const nodes = await (await nodeModel.find(nodeQuery)).toArray();
  if (nodes.length === 0) {
    console.error(`No node found with query ${nodeQuery}. Skipped.`);
    return;
  }
  if (nodes.length > 1) {
    console.error(`Found more than one nodes with ${nodeQuery}. Skipped.`);
    return;
  }

  const panoImages = await (await panoImageModel.find(panoImageQuery)).toArray();
  if (panoImages.length === 0) {
    console.error(`No panoImage found with query ${panoImageQuery}. Skipped.`);
    return;
  }
  if (panoImages.length > 1) {
    console.error(`Found more than one panoImages with ${panoImageQuery}. Skipped.`);
    return;
  }

  try {
    await nodeModel.updateOne({ _id: nodes[0]._id }, { panoImage: panoImages[0]._id });
  } catch (err) {
    console.error(
      `MongoDB failed to insert panoImage ${panoImages[0]._id} to node ${nodes[0]._id}`,
    );
  }
}

module.exports = { updateNodePanoImage };
