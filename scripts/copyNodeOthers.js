'use strict';

const loadEnv = require('../env');
const node = require('../models/nodes');
const nodeOthers = require('../models/nodeOthers');

loadEnv();

async function copyNodeOthers() {
  const items = await nodeOthers.find();
  while (await items.hasNext()) {
    const item = await items.next();

    await node.updateOneNoValidate({ _id: item._id }, { $set: { others: item.others } });
  }

  console.log('Copied successfully.');
  process.exit(0);
}

copyNodeOthers();
