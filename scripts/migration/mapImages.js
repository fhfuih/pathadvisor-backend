'use strict';

const { RAW_DATA_PATH } = process.env;
// eslint-disable-next-line
const floorsData = require(`${process.cwd()}/${RAW_DATA_PATH}/floors.js`);
const fs = require('fs');
const saveAndResizeMapImage = require('../../routes/mapImages/saveAndResizeMapImage');

async function migrate() {
  for (const floorId of Object.keys(floorsData)) {
    try {
      const newFloorId = floorId.replace('NAB', 'LSK');
      const imageBuffer = fs.readFileSync(`${RAW_DATA_PATH}/map_image/${floorId}/${floorId}.png`);

      await saveAndResizeMapImage(newFloorId, imageBuffer);
      console.log(`Inserted map image for ${floorId}`);
    } catch (err) {
      console.error(`Error inserting map images for ${floorId}`, err);
    }
  }

  console.log('Done');
  process.exit();
}

migrate();
