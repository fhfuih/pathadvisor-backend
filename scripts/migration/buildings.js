'use strict';

const { RAW_DATA_PATH } = process.env;
const buildings = require('../../models/buildings');

// eslint-disable-next-line
const buildingsData = require(`${process.cwd()}/${RAW_DATA_PATH}/buildings.js`);

async function migrate() {
  await Promise.all(
    Object.keys(buildingsData).map(async buildingId => {
      try {
        await buildings.insertOne({ name: buildingsData[buildingId].name, _id: buildingId });
        console.log(`Inserted building ${buildingId}`);
      } catch (err) {
        console.error(`Error inserting building with _id ${buildingId}`, err);
      }
    }),
  );

  console.log('Done');
  process.exit();
}

migrate();
