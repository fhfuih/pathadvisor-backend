'use strict';

const fs = require('fs');
const connectors = require('../../models/connectors');

const { RAW_DATA_PATH } = process.env;

function getTypeFromId(id) {
  switch (true) {
    case id.startsWith('DOOR'):
      return 'crossBuildingConnector';
    case id.startsWith('STAIR'):
      return 'stair';
    case id.startsWith('ESCALATOR'):
      return 'escalator';
    case id.startsWith('LIFT'):
      return 'lift';
    default:
      throw new Error(`Unable to infer connector type from id ${id}`);
  }
}

const knownBuildingPrefixes = ['LSK', 'UC', 'IAS', 'CYT'];

function getFloorInteger(floorWithPrefix) {
  if (floorWithPrefix === 'Overall') {
    return 65535;
  }

  let floor = floorWithPrefix;

  knownBuildingPrefixes.forEach(prefix => {
    floor = floor.replace(prefix, '');
  });

  floor = floor.replace('LG', '-');

  if (floor === 'G') {
    return 0;
  }

  if (floor === 'UG') {
    return 0.5;
  }

  const intFloor = parseInt(floor, 10);

  if (Number.isNaN(intFloor)) {
    throw new Error(`Cannot parse floor ${floorWithPrefix} > ${floor} to integer`);
  }

  return intFloor;
}

function sortFloor(a, b) {
  return getFloorInteger(b) - getFloorInteger(a);
}

async function migrate() {
  // eslint-disable-next-line
  const floorsData = require(`${process.cwd()}/${RAW_DATA_PATH}/floors.js`);

  const connectorsById = Object.keys(floorsData).reduce((connectorsByIdAccumulator, floor) => {
    const connectorTextContent = fs.readFileSync(`${RAW_DATA_PATH}/connectors_${floor}.xml`, {
      encoding: 'utf8',
    });

    connectorTextContent
      .split('\n')
      .filter(line => Boolean(line.trim()))
      .forEach(line => {
        const [id, , weight, floorIds] = line.split(';');

        const newConnectorId = id.replace('NAB', 'LSK');
        if (!connectorsByIdAccumulator[newConnectorId]) {
          // eslint-disable-next-line no-param-reassign
          connectorsByIdAccumulator[newConnectorId] = {
            id: newConnectorId,
            tagIds: [getTypeFromId(newConnectorId)],
            ...(parseInt(weight, 10) ? { weight: parseInt(weight, 10) } : {}),
            floorIds: [],
            ignoreMinLiftRestriction: newConnectorId.startsWith('ESCALATOR_SP'),
          };
        }

        // eslint-disable-next-line no-param-reassign
        connectorsByIdAccumulator[newConnectorId].floorIds = [
          ...new Set([
            ...connectorsByIdAccumulator[newConnectorId].floorIds,
            ...floorIds
              .split(',')
              .map(v => v.trim())
              .map(v => v.replace('NAB', 'LSK'))
              .filter(v => v),
          ]),
        ];
      });

    return connectorsByIdAccumulator;
  }, {});

  await Promise.all(
    Object.values(connectorsById).map(async ({ id, floorIds, ...others }) => {
      try {
        await connectors.insertOne({
          _id: id,
          floorIds: floorIds.sort(sortFloor),
          ...others,
        });
        console.log(`Inserted connector with _id ${id}`);
      } catch (err) {
        console.error(`Error inserting connector with _id ${id}`, err);
      }
    }),
  );

  console.log('Done');
  process.exit();
}

migrate();
