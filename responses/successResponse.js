'use strict';

module.exports = function success({ data, meta }) {
  return { data, meta };
};
