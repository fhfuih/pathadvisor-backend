'use strict';

function errorMessage({ code, message, validationErrors, stack }) {
  return {
    error: {
      code,
      message,
      validationErrors,
      stack: process.env.NODE_ENV !== 'production' ? stack : undefined,
    },
  };
}

module.exports = errorMessage;
