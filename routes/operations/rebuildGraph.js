'use strict';

const buildGraph = require('../../graph/buildGraph');
const cache = require('../../graph/cache');
const successResponse = require('../../responses/successResponse');

async function rebuildGraph(req, res) {
  const { graph, nodesById, connectorsById, settings: settingData } = await buildGraph();
  cache.data = { graph, nodesById, connectorsById, settings: settingData };

  res.json(successResponse({ data: { success: true } }));
}

module.exports = rebuildGraph;
