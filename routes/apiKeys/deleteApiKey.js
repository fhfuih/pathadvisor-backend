'use strict';

const ObjectId = require('../../utils/ObjectId');
const successResponse = require('../../responses/successResponse');
const apiKeys = require('../../models/apiKeys');

async function deleteApiKey(req, res) {
  const _id = ObjectId(req.params.id);
  await apiKeys.deleteOne({ _id });
  res.json(successResponse({ meta: { deleted: true } }));
}

module.exports = deleteApiKey;
