'use strict';

const router = require('express-promise-router')();
const initDataCache = require('./cache');

async function initData(req, res) {
  res.json(initDataCache.data);
}

router.get('/init-data', initData);

module.exports = router;
