'use strict';

function getImageUrl(req, tag) {
  const { API_PREFIX = `${req.protocol}://${req.get('host')}${req.baseUrl}` } = process.env;
  return `${API_PREFIX}/tags/${tag._id}.png`;
}

module.exports = getImageUrl;
