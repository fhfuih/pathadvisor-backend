'use strict';

const router = require('express-promise-router')();
const bodyParser = require('body-parser');

const findTags = require('./findTags');
const findTagImage = require('./findTagImage');
const upsertTag = require('./upsertTag');
const deleteTag = require('./deleteTag');
const checkPermissions = require('../../middlewares/auth');
const permissions = require('../../auth/permissions');
const uploadImage = require('./uploadImage');

router.get('/tags', findTags);
router.get('/tags/:id.png', findTagImage);
router.post(
  '/tags/:id',
  checkPermissions([permissions['TAG:INSERT'], permissions['TAG:UPDATE']]),
  upsertTag,
);
router.post(
  '/tags/:id/data',
  checkPermissions([permissions['TAG:UPDATE']]),
  bodyParser.raw(),
  uploadImage,
);
router.delete('/tags/:id', checkPermissions([permissions['TAG:DELETE']]), deleteTag);

module.exports = router;
