'use strict';

const tags = require('../../models/tags');
const NotFoundError = require('../../errors/NotFoundError');

async function findTagImage(req, res) {
  const _id = req.params.id;
  const data = await tags.findOne({ _id });

  if (!data) {
    throw new NotFoundError({ _id }, 'Legend not found');
  }
  if (!data.data || !data.data.buffer) {
    throw new NotFoundError({ _id }, 'Image buffer not found in database');
  }

  res.set('Content-Type', 'image/png');
  res.send(data.data.buffer);
}

module.exports = findTagImage;
