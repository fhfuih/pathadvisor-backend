'use strict';

const panoImages = require('../../models/panoImages');
const ObjectId = require('../../utils/ObjectId');
const successResponse = require('../../responses/successResponse');
const NotFoundError = require('../../errors/NotFoundError');

async function findPanoImageInfo(req, res) {
  const { id } = req.params;

  const query = { _id: new ObjectId(id) };
  const data = await panoImages.findOne(query, { projection: { data: 0 } });

  if (!data) {
    throw new NotFoundError(query, 'Panorama Image not found');
  }

  res.json(successResponse({ data }));
}

module.exports = findPanoImageInfo;
