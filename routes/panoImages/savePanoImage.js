'use strict';

const { Binary } = require('mongodb');

const sharp = require('sharp');

const panoImageModel = require('../../models/panoImages');

/**
 * Save a pano image object to the database
 * @param {Object} document The panoImage document to insert
 * @param {Number} document.westX WestX
 * @param {String} document.name Name
 * @param {Number} document.lastUpdatedAt Last updated timestamp
 * @param {Buffer} document.data Image buffer
 * @param {Boolean} compress Whether to compress the image or not
 * @returns {Object} The inserted object with _id
 */
async function savePanoImage(document, compress = false) {
  const lastUpdatedAt = document.lastUpdatedAt || new Date().getTime();

  const buf = compress
    ? await sharp(document.data)
        .jpeg({ quality: 80 })
        .toBuffer()
    : document.data;

  return panoImageModel.insertOne({
    ...document,
    lastUpdatedAt,
    data: new Binary(buf),
  });
}

module.exports = savePanoImage;
