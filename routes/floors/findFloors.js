'use strict';

const successResponse = require('../../responses/successResponse');
const floors = require('../../models/floors');

async function findFloors(req, res) {
  const data = await (await floors.find()).toArray();
  res.json(successResponse({ data, meta: { count: data.length } }));
}

module.exports = findFloors;
