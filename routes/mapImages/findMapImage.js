'use strict';

const mapImages = require('../../models/mapImages');
const NotFoundError = require('../../errors/NotFoundError');

async function findMapImage(req, res) {
  const { floorId } = req.params;

  const query = { floorId };
  const [data] = await (await mapImages.find(query, {
    limit: 1,
    sort: { lastUpdatedAt: -1 },
  })).toArray();

  if (!data) {
    throw new NotFoundError(query, 'Map image not found');
  }
  if (!data.data || !data.data.buffer) {
    throw new NotFoundError(query, 'Image buffer not found in database');
  }

  res.set('Content-Type', 'image/png');
  res.send(data.data.buffer);
}

module.exports = findMapImage;
