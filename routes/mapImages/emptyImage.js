'use strict';

const sharp = require('sharp');
const { MAP_TILE_WIDTH, MAP_TILE_HEIGHT, BACKGROUND_COLOR } = require('./imageMeta');

let image;

async function emptyImage() {
  if (!image) {
    image = await sharp(null, {
      create: {
        width: MAP_TILE_WIDTH,
        height: MAP_TILE_HEIGHT,
        channels: 4,
        background: BACKGROUND_COLOR,
      },
    })
      .png()
      .toBuffer();
  }

  return image;
}
module.exports = emptyImage;
