'use strict';

const MAP_TILE_WIDTH = 200;
const MAP_TILE_HEIGHT = 200;
const BACKGROUND_COLOR = { r: 250, g: 250, b: 250, alpha: 1 };

module.exports = {
  MAP_TILE_WIDTH,
  MAP_TILE_HEIGHT,
  BACKGROUND_COLOR,
};
