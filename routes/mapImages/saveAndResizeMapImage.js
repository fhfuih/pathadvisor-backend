'use strict';

const { Binary } = require('mongodb');

const sharp = require('sharp');

const mapImages = require('../../models/mapImages');
const mapTiles = require('../../models/mapTiles');
const settingModel = require('../../models/settings');
const floors = require('../../models/floors');

const { MAP_TILE_WIDTH, MAP_TILE_HEIGHT, BACKGROUND_COLOR } = require('./imageMeta');

const defaultExtend = { left: 0, right: 0, top: 0, bottom: 0, background: BACKGROUND_COLOR };

function extractAndExtend(sharpImage, { startX, startY, imageWidth, imageHeight, left, top }) {
  const x = left - startX;
  const y = top - startY;

  const extract = {
    left: x > 0 ? x : 0,
    top: y > 0 ? y : 0,
  };

  const extend = {};

  if (x < 0) {
    extend.left = startX - left;
    extract.width = MAP_TILE_WIDTH - extend.left;
  } else if (x + MAP_TILE_WIDTH > imageWidth) {
    extract.width = imageWidth - x;
    extend.right = x + MAP_TILE_WIDTH - imageWidth;
  } else {
    extract.width = MAP_TILE_WIDTH;
  }

  if (y < 0) {
    extend.top = startY - top;
    extract.height = MAP_TILE_HEIGHT - extend.top;
  } else if (y + MAP_TILE_HEIGHT > imageHeight) {
    extract.height = imageHeight - y;
    extend.bottom = y + MAP_TILE_HEIGHT - imageHeight;
  } else {
    extract.height = MAP_TILE_HEIGHT;
  }

  return sharpImage.extract(extract).extend({ ...defaultExtend, ...extend });
}

async function saveAndResizeMapImage(floorId, imageBuffer) {
  const [settings] = await (await settingModel.find(
    {},
    { limit: 1, sort: { $natural: -1 } },
  )).toArray();

  if (!settings) {
    throw new Error('No setting is found');
  }

  if (!settings.levelToScale) {
    throw new Error('No levelToScale is defined in setting');
  }

  const floor = await floors.findOne({ _id: floorId });

  if (!floor) {
    throw new Error('Floor data not found');
  }

  const lastUpdatedAt = new Date().getTime();

  await mapImages.insertOne({ floorId, data: new Binary(imageBuffer), lastUpdatedAt });

  // Split into map tiles
  const pngImage = sharp(imageBuffer);
  const { width, height, channels } = await pngImage.metadata();

  const image = sharp(await pngImage.raw().toBuffer(), { raw: { width, height, channels } });

  for (const [zoomLevel, scale] of settings.levelToScale.entries()) {
    console.log({ floorId, zoomLevel });

    let { startX, startY } = floor;
    startX = Math.round(startX * scale);
    startY = Math.round(startY * scale);

    const startGridX = Math.floor(startX / MAP_TILE_WIDTH) * MAP_TILE_WIDTH;
    const startGridY = Math.floor(startY / MAP_TILE_HEIGHT) * MAP_TILE_HEIGHT;
    const resizeWidth = Math.round(width * scale);
    const resizeHeight = Math.round(height * scale);

    const endGridX =
      resizeWidth + startX > 0
        ? Math.ceil((resizeWidth + startX) / MAP_TILE_WIDTH) * MAP_TILE_WIDTH
        : Math.ceil((startGridX - resizeWidth) / MAP_TILE_WIDTH) * MAP_TILE_WIDTH;
    const endGridY =
      resizeHeight + startY > 0
        ? Math.ceil((resizeHeight + startY) / MAP_TILE_HEIGHT) * MAP_TILE_HEIGHT
        : Math.ceil((startGridY - resizeHeight) / MAP_TILE_HEIGHT) * MAP_TILE_HEIGHT;

    let resizedImage = image;

    if (scale !== 1) {
      resizedImage = sharp(
        await image
          .resize({
            width: resizeWidth,
            height: resizeHeight,
          })
          .raw()
          .toBuffer(),
        {
          raw: {
            width: resizeWidth,
            height: resizeHeight,
            channels,
          },
        },
      );
    }

    let x = startGridX;
    while (x < endGridX) {
      let y = startGridY;
      while (y < endGridY) {
        let pipeline = resizedImage.clone();
        pipeline = extractAndExtend(pipeline, {
          left: x,
          top: y,
          startX,
          startY,
          imageWidth: resizeWidth,
          imageHeight: resizeHeight,
        });

        const data = await pipeline.png().toBuffer();
        await mapTiles.insertOne({
          floorId,
          x,
          y,
          zoomLevel,
          data: new Binary(data),
          lastUpdatedAt,
        });
        y += MAP_TILE_HEIGHT;
      }
      x += MAP_TILE_WIDTH;
    }
  }
}

module.exports = saveAndResizeMapImage;
