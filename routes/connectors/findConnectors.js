'use strict';

const connectors = require('../../models/connectors');
const successResponse = require('../../responses/successResponse');

async function findConnectors(req, res) {
  const data = await (await connectors.find()).toArray();

  res.json(successResponse({ data, meta: { count: data.length } }));
}

module.exports = findConnectors;
