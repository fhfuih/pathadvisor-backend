'use strict';

const router = require('express-promise-router')();

const findConnectors = require('./findConnectors');
const findConnectorById = require('./findConnectorById');
const upsertConnector = require('./upsertConnector');
const deleteConnector = require('./deleteConnector');
const checkPermissions = require('../../middlewares/auth');
const permissions = require('../../auth/permissions');

router.get('/connectors', checkPermissions([permissions['CONNECTOR:LIST']]), findConnectors);
router.get('/connectors/:id', findConnectorById);
router.post(
  '/connectors/:id',
  checkPermissions([permissions['CONNECTOR:INSERT']]),
  upsertConnector,
);
router.delete(
  '/connectors/:id',
  checkPermissions([permissions['CONNECTOR:DELETE']]),
  deleteConnector,
);

module.exports = router;
