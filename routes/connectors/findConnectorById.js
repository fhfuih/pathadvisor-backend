'use strict';

const connectors = require('../../models/connectors');
const errorResponse = require('../../responses/errorResponse');
const successResponse = require('../../responses/successResponse');

async function findConnectorById(req, res) {
  const data = await connectors.findOne(
    { _id: req.params.id },
    { projection: { ignoreMinLiftRestriction: 0 } },
  );

  if (!data) {
    res.status(404).json(errorResponse({ message: 'Connector not found' }));
    return;
  }

  res.json(successResponse({ data }));
}

module.exports = findConnectorById;
