'use strict';

const successResponse = require('../../responses/successResponse');
const errorResponse = require('../../responses/errorResponse');
const settings = require('../../models/settings');

async function findSettings(req, res) {
  const data = await settings.findOne({}, { sort: { $natural: -1 } });

  if (!data) {
    res.status(404).json(errorResponse({ message: 'Settings not found' }));
    return;
  }

  res.json(successResponse({ data }));
}

module.exports = findSettings;
