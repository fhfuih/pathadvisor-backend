'use strict';

const successResponse = require('../../responses/successResponse');
const edges = require('../../models/edges');

async function deleteEdge(req, res) {
  const _id = req.params.id;
  await edges.deleteOne({ _id });
  res.json(successResponse({ meta: { deleted: true } }));
}

module.exports = deleteEdge;
