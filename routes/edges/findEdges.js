'use strict';

const edges = require('../../models/edges');
const successResponse = require('../../responses/successResponse');

async function findEdges(req, res) {
  const { floorId } = req.params;
  const data = await (await edges.find({ floorId })).toArray();

  res.json(successResponse({ data, meta: { count: data.length } }));
}

module.exports = findEdges;
