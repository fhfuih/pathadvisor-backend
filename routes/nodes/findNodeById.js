'use strict';

const nodes = require('../../models/nodes');
const successResponse = require('../../responses/successResponse');
const NotFoundError = require('../../errors/NotFoundError');
const transformNodeResponse = require('./transformNodeResponse');

async function findNodeById(req, res) {
  const { id } = req.params;
  const query = { _id: id };

  const node = await nodes.findOne(query, { projection: { polygonCoordinates: 0 } });

  if (!node) {
    throw new NotFoundError(query, 'Node not found');
  }

  res.json(
    successResponse({
      data: transformNodeResponse(req, node),
    }),
  );
}

module.exports = findNodeById;
