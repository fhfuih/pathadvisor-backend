'use strict';

const Ajv = require('ajv');

const validator = new Ajv({ allErrors: true });

const nodesModel = require('../../models/nodes');
const successResponse = require('../../responses/successResponse');
const ValidationError = require('../../errors/ValidationError');
const transformNodeResponse = require('./transformNodeResponse');

async function findPanoNodes(req, res) {
  const { floorId } = req.params;

  const querySchema = {
    definitions: {},
    $schema: 'http://json-schema.org/draft-07/schema#',
    type: 'object',
    additionalProperties: false,
    required: ['floorId'],
    properties: {
      floorId: {
        $id: '#/properties/floorId',
        type: 'string',
        minLength: 1,
      },
    },
  };

  const validate = validator.compile(querySchema);

  if (!validate({ floorId })) {
    throw new ValidationError(validate.errors, 'Query string validation errors');
  }

  const data = await (await nodesModel.find(
    {
      floorId,
      panoImage: { $exists: true },
    },
    { projection: { polygonCoordinates: 0 } },
  )).toArray();

  res.json(
    successResponse({
      data: data.map(node => transformNodeResponse(req, node)),
      meta: { count: data.length },
    }),
  );
}

module.exports = findPanoNodes;
