'use strict';

const successResponse = require('../../responses/successResponse');
const nodes = require('../../models/nodes');
const errorResponse = require('../../responses/errorResponse');
const edges = require('../../models/edges');
const panoEdges = require('../../models/panoEdges');

async function deleteNode(req, res) {
  const _id = req.params.id;

  const edge = await edges.findOne({ $or: [{ fromNodeId: _id }, { toNodeId: _id }] });
  const panoEdge = await panoEdges.findOne({ $or: [{ fromNodeId: _id }, { toNodeId: _id }] });

  if (edge || panoEdge) {
    res.status(400).json(
      errorResponse({
        message: `Cannot delete target node because it is still connected by at least one or more edge`,
      }),
    );
    return;
  }

  await nodes.deleteOne({ _id });
  res.json(successResponse({ meta: { deleted: true } }));
}

module.exports = deleteNode;
