'use strict';

const nodes = require('../../models/nodes');
const successResponse = require('../../responses/successResponse');
const transformNodeResponse = require('./transformNodeResponse');

async function findNodes(req, res) {
  const { floorId } = req.params;
  const data = await (await nodes.find(
    { floorId },
    { projection: { polygonCoordinates: 0 } },
  )).toArray();

  res.json(
    successResponse({
      data: data.map(node => transformNodeResponse(req, node)),
      meta: { count: data.length },
    }),
  );
}

module.exports = findNodes;
