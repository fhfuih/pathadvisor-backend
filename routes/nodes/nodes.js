'use strict';

const router = require('express-promise-router')();

const findNodes = require('./findNodes');
const findNodeById = require('./findNodeById');
const findNodesByKeyword = require('./findNodesByKeyword');
const findNodesWithinBox = require('./findNodesWithinBox');
const findNodeNearCoordinates = require('./findNodeNearCoordinates');
const findPanoNodes = require('./findPanoNodes');
const findPanoNodeNearCoordinates = require('./findPanoNodeNearCoordinates');
const findNextPanoNode = require('./findNextPanoNode');
const upsertNode = require('./upsertNode');
const updateNodeOthers = require('./updateNodeOthers');
const removeNode = require('./deleteNode');
const checkPermissions = require('../../middlewares/auth');
const permissions = require('../../auth/permissions');

router.get('/nodes', findNodesByKeyword);
router.get('/nodes/:id', findNodeById);
router.get(
  '/floors/:floorId/nodes',
  async (req, res, next) => {
    if (Object.keys(req.query).length === 0) {
      await checkPermissions([permissions['NODE:LIST']])(req, res, next);
      return;
    }
    next();
  },
  async (req, res) => {
    if (Object.keys(req.query).length === 0) {
      await findNodes(req, res);
      return;
    }
    await findNodesWithinBox(req, res);
  },
);
router.get('/floors/:floorId/node', findNodeNearCoordinates);
router.get('/pano/floors/:floorId/node', findPanoNodeNearCoordinates);
router.get('/pano/floors/:floorId/nodes', findPanoNodes);
router.get('/pano/next', findNextPanoNode);
router.post('/nodes', checkPermissions([permissions['NODE:INSERT']]), upsertNode('INSERT'));
router.post('/nodes/:id', checkPermissions([permissions['NODE:UPDATE']]), upsertNode('UPDATE'));
router.post(
  '/nodes/:id/others',
  checkPermissions([permissions['NODE:UPDATE:OTHERS']]),
  updateNodeOthers,
);
router.delete('/nodes/:id', checkPermissions([permissions['NODE:DELETE']]), removeNode);

module.exports = router;
