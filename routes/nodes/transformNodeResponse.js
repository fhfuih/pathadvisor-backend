'use strict';

const getCenterCoordinates = require('./getCenterCoordinates');
const getImageUrl = require('../images/getImageUrl');
const getPanoImageUrl = require('../panoImages/getImageUrl');

function transformNodeResponse(req, node) {
  return {
    ...node,
    ...(node.geoLocs ? getCenterCoordinates(node.geoLocs) : {}),
    ...(node.image ? { imageUrl: getImageUrl(req, node.image) } : {}),
    ...(node.panoImage ? { panoImageUrl: getPanoImageUrl(req, node.panoImage) } : {}),
  };
}

module.exports = transformNodeResponse;
