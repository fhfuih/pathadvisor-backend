'use strict';

const Ajv = require('ajv');

const validator = new Ajv({ allErrors: true });

const keyBy = require('lodash.keyby');
const nodeModel = require('../../models/nodes');

const successResponse = require('../../responses/successResponse');
const breadthFirstSearch = require('../../graph/breadthFirstSearch');
const convertLiteralToBoolean = require('../../utils/convertLiteralToBoolean');
const ValidationError = require('../../errors/ValidationError');
const NotFoundError = require('../../errors/NotFoundError');
const transformNodeResponse = require('../nodes/transformNodeResponse');

const { MAX_WEIGHT, CONNECTOR_SMALLEST_WEIGHT } = require('../../graph/constants');

const MODES = {
  SHORTEST_TIME: 'SHORTEST_TIME',
  SHORTEST_DISTANCE: 'SHORTEST_DISTANCE',
  MIN_NO_OF_LIFTS: 'MIN_NO_OF_LIFTS',
};

const DISTANCE_UNIT = {
  MINUTE: 'MINUTE',
  METER: 'METER',
};

const PATH_NOT_FOUND_MESSAGE = 'No path found';

async function searchShortestPath(req, res) {
  const { graph, nodesById, connectorsById, settings } = req;

  if (!graph) {
    throw new Error('No graph is found');
  }

  if (!nodesById) {
    throw new Error('nodesById not found');
  }

  if (!connectorsById) {
    throw new Error('connectorsById not found');
  }

  const querySchema = {
    definitions: {},
    $schema: 'http://json-schema.org/draft-07/schema#',
    type: 'object',
    required: ['fromId', 'toId'],
    properties: {
      fromId: {
        $id: '#/properties/fromId',
        type: 'string',
        minLength: 1,
      },
      toId: {
        $id: '#/properties/toId',
        type: 'string',
        minLength: 1,
      },
      mode: {
        $id: '#/properties/mode',
        type: 'string',
        enum: Object.values(MODES),
      },
      noStairCase: {
        $id: '#/properties/noStairCase',
        type: 'string',
        enum: ['true', 'false'],
      },
      noEscalator: {
        $id: '#/properties/noEscalator',
        type: 'string',
        enum: ['true', 'false'],
      },
    },
  };

  const validate = validator.compile(querySchema);

  const { fromId, toId, mode = MODES.SHORTEST_TIME } = req.query;
  let { noStairCase, noEscalator } = req.query;

  if (!validate({ fromId, toId, mode, noStairCase, noEscalator })) {
    throw new ValidationError(validate.errors, 'Query string validation errors');
  }

  noStairCase = noStairCase ? convertLiteralToBoolean(noStairCase) : true;
  noEscalator = noEscalator ? convertLiteralToBoolean(noEscalator) : false;

  if (!graph[fromId]) {
    throw new NotFoundError({}, PATH_NOT_FOUND_MESSAGE);
  }

  const { prev, dist } = breadthFirstSearch(
    graph,
    fromId,
    ({ prevNodeId, nodeId, weight, isConnectorEdge }) => {
      const { tags, connectorId } = nodesById[nodeId];
      const { connectorId: prevConnectorId } = nodesById[prevNodeId];

      if (noEscalator && isConnectorEdge && tags && tags.escalator) {
        return { shouldSkip: true };
      }

      if (noStairCase && isConnectorEdge && tags && tags.stair) {
        return { shouldSkip: true };
      }

      if (
        mode === MODES.MIN_NO_OF_LIFTS &&
        connectorId &&
        connectorId === prevConnectorId &&
        !connectorsById[connectorId].ignoreMinLiftRestriction &&
        (tags.lift || tags.escalator || tags.crossBuildingConnector)
      ) {
        return { weight: MAX_WEIGHT };
      }

      if (mode === MODES.SHORTEST_DISTANCE && connectorId && connectorId === prevConnectorId) {
        return { weight: CONNECTOR_SMALLEST_WEIGHT };
      }

      return { weight };
    },
  );

  const pathNodeIds = [toId];
  let prevNodeId = toId;
  while (prev[prevNodeId]) {
    pathNodeIds.push(prev[prevNodeId]);
    prevNodeId = prev[prevNodeId];
  }

  if (prevNodeId !== fromId) {
    throw new NotFoundError({}, PATH_NOT_FOUND_MESSAGE);
  }

  const nodes = await (await nodeModel.find(
    { _id: { $in: pathNodeIds } },
    { projection: { polygonCoordinates: 0, geoLocs: 0, keywords: 0, others: 0 } },
  )).toArray();

  const fullNodesById = keyBy(nodes, o => o._id);
  const data = pathNodeIds
    .map((nodeId, i) => {
      const nextNodeId = i + 1 >= pathNodeIds.length ? null : pathNodeIds[i + 1];
      const nextDist = !nextNodeId ? 0 : dist[nextNodeId];
      const node = fullNodesById[nodeId];
      const nextNode = !nextNodeId ? null : fullNodesById[nextNodeId];
      const isTravellingInLift =
        nextNode &&
        node.connectorId &&
        node.connectorId === nextNode.connectorId &&
        nodesById[nodeId].tags.lift;
      let distance = dist[nodeId] - nextDist;

      if (isTravellingInLift) {
        distance = mode === MODES.MIN_NO_OF_LIFTS ? 0 : distance * settings.minutesPerMeter;
      }

      if (distance >= MAX_WEIGHT) {
        distance = 0;
      }

      return {
        ...transformNodeResponse(req, node),
        distance,
        unit: isTravellingInLift ? DISTANCE_UNIT.MINUTE : DISTANCE_UNIT.METER,
      };
    })
    .reverse();
  res.json(successResponse({ data }));
}

module.exports = searchShortestPath;
