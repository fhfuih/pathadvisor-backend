'use strict';

const Ajv = require('ajv');

const validator = new Ajv({ allErrors: true });
const successResponse = require('../../responses/successResponse');
const nodeModel = require('../../models/nodes');
const nodeKeywordQuery = require('../nodes/nodeKeywordQuery');
const transformNodeResponse = require('../nodes/transformNodeResponse');
const ValidationError = require('../../errors/ValidationError');
const NotFoundError = require('../../errors/NotFoundError');
const initDataCache = require('../initData/cache');

const convertLiteralToBoolean = require('../../utils/convertLiteralToBoolean');
const breadthFirstSearch = require('../../graph/breadthFirstSearch');
const { INF } = require('../../graph/constants');

const querySchema = {
  definitions: {},
  $schema: 'http://json-schema.org/draft-07/schema#',
  type: 'object',
  oneOf: [{ required: ['type', 'startId'] }, { required: ['type', 'startName'] }],
  properties: {
    startId: {
      $id: '#/properties/startId',
      type: 'string',
      minLength: 1,
    },
    startName: {
      $id: '#/properties/startName',
      type: 'string',
      minLength: 1,
    },
    sameFloor: {
      $id: '#/properties/sameFloor',
      type: 'string',
      enum: ['true', 'false'],
    },
    type: {
      $id: '#/properties/type',
      type: 'string',
      minLength: 1,
    },
  },
};

const validate = validator.compile(querySchema);

const NO_NEAREST_ITEM_FOUND_MESSAGE = 'No nearest item found';

async function searchNearestItem(req, res) {
  const { graph, nodesById, connectorsById } = req;

  if (!graph) {
    throw new Error('No graph is found');
  }

  if (!nodesById) {
    throw new Error('nodesById not found');
  }

  if (!connectorsById) {
    throw new Error('connectorsById not found');
  }

  const { startName, type } = req.query;
  let { startId, sameFloor } = req.query;
  const {
    data: { floors, buildingIds },
  } = initDataCache;

  if (!validate({ startId, startName, type, sameFloor })) {
    throw new ValidationError(validate.errors, 'Query string validation errors');
  }

  sameFloor = sameFloor ? convertLiteralToBoolean(sameFloor) : true;

  const startNodes = await (await nodeModel.find(
    startName ? nodeKeywordQuery(startName) : { _id: startId },
    {
      projection: { polygonCoordinates: 0 },
    },
  )).toArray();

  // If there more than one result, sort it by the rank of buildings
  if (startNodes.length > 1) {
    startNodes.sort(
      (a, b) =>
        buildingIds.indexOf(floors[a.floorId].buildingId) -
        buildingIds.indexOf(floors[b.floorId].buildingId),
    );
  }

  const startNode = startNodes[0];

  if (!startNode) {
    throw new NotFoundError({}, NO_NEAREST_ITEM_FOUND_MESSAGE);
  }

  startId = startNode._id;

  if (!graph[startId]) {
    throw new NotFoundError({}, NO_NEAREST_ITEM_FOUND_MESSAGE);
  }

  const startNodeFloor = nodesById[startId].floorId;

  const { prev, dist } = breadthFirstSearch(graph, startId, ({ nodeId, weight }) => {
    if (sameFloor && nodesById[nodeId].floorId !== startNodeFloor) {
      return {
        shouldSkip: true,
      };
    }

    return { weight };
  });

  let minDist = INF;
  let nearestNodeId = null;

  Object.keys(prev).forEach(nodeId => {
    if (nodeId === startId) {
      return;
    }

    if (nodesById[nodeId].tags[type] && dist[nodeId] < minDist) {
      minDist = dist[nodeId];
      nearestNodeId = nodeId;
    }
  });

  if (!nearestNodeId) {
    throw new NotFoundError({}, NO_NEAREST_ITEM_FOUND_MESSAGE);
  }

  const nearestItem = await nodeModel.findOne(
    { _id: nearestNodeId },
    { projection: { polygonCoordinates: 0 } },
  );
  res.status(200).json(
    successResponse({
      data: {
        nearestItem: transformNodeResponse(req, nearestItem),
        from: transformNodeResponse(req, startNode),
      },
    }),
  );
}

module.exports = searchNearestItem;
