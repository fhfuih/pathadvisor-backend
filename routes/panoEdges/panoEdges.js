'use strict';

const router = require('express-promise-router')();
const findPanoEdges = require('./findPanoEdges');
const insertPanoEdge = require('./insertPanoEdge');
const deletePanoEdge = require('./deletePanoEdge');
const findPanoEdgeCoordinates = require('./findPanoEdgeCoordinates');
const checkPermissions = require('../../middlewares/auth');
const permissions = require('../../auth/permissions');

router.get('/pano/floors/:floorId/edge-coords', findPanoEdgeCoordinates);
router.get(
  '/pano/floors/:floorId/edges',
  checkPermissions([permissions['PANO_EDGE:LIST']]),
  findPanoEdges,
);
router.post('/pano/edges', checkPermissions([permissions['PANO_EDGE:INSERT']]), insertPanoEdge);
router.delete(
  '/pano/edges/:id',
  checkPermissions([permissions['PANO_EDGE:DELETE']]),
  deletePanoEdge,
);

module.exports = router;
