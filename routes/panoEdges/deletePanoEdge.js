'use strict';

const successResponse = require('../../responses/successResponse');
const panoEdges = require('../../models/panoEdges');

async function deletePanoEdge(req, res) {
  const _id = req.params.id;
  await panoEdges.deleteOne({ _id });
  res.json(successResponse({ meta: { deleted: true } }));
}

module.exports = deletePanoEdge;
