'use strict';

const keyBy = require('lodash.keyby');
const nodeModel = require('../../models/nodes');
const panoEdgeModel = require('../../models/panoEdges');
const successResponse = require('../../responses/successResponse');

async function findPanoEdgeCoordinates(req, res) {
  const { floorId } = req.params;

  const panoNodes = await (await nodeModel.find(
    {
      floorId,
      panoImage: { $exists: true },
    },
    { projection: { polygonCoordinates: 0 } },
  )).toArray();
  const panoNodesById = keyBy(panoNodes, '_id');

  const panoEdges = await (await panoEdgeModel.find({ floorId })).toArray();

  const toNodeIdsbyFromNodeId = {};
  const uniDirPanoEdges = panoEdges.reduce((carrier, panoEdge) => {
    const { fromNodeId, toNodeId } = panoEdge;
    if (
      (toNodeIdsbyFromNodeId[fromNodeId] && toNodeIdsbyFromNodeId[fromNodeId].has(toNodeId)) || 
      (toNodeIdsbyFromNodeId[toNodeId] && toNodeIdsbyFromNodeId[toNodeId].has(fromNodeId))
    ) {
      return carrier;
    }

    if (toNodeIdsbyFromNodeId[fromNodeId]) {
      toNodeIdsbyFromNodeId[fromNodeId].add(toNodeId);
    } else if (toNodeIdsbyFromNodeId[toNodeId]) {
      toNodeIdsbyFromNodeId[toNodeId].add(fromNodeId);
    } else {
      toNodeIdsbyFromNodeId[fromNodeId] = new Set([toNodeId]);
    }
    return [...carrier, panoEdge];
  }, []);

  const data = uniDirPanoEdges.map(({ _id, fromNodeId, toNodeId }) => ({
    _id,
    floorId,
    fromNodeCoordinates: panoNodesById[fromNodeId].coordinates,
    toNodeCoordinates: panoNodesById[toNodeId].coordinates,
  }));

  res.json(successResponse({ data, meta: { count: data.length } }));
}

module.exports = findPanoEdgeCoordinates;
