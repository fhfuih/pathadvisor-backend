'use strict';

const { Binary } = require('mongodb');
const resources = require('../../models/resources');
const successResponse = require('../../responses/successResponse');
const getResourceUrl = require('./getResourceUrl');
const ValidationError = require('../../errors/ValidationError');

async function uploadResource(req, res) {
  const buffer = req.body;

  if (!(buffer instanceof Buffer)) {
    throw new ValidationError(
      null,
      'No request body received. Make sure you set content-type to application/octet-stream',
    );
  }

  const lastUpdatedAt = new Date().getTime();

  const { _id } = await resources.insertOne({
    data: new Binary(buffer),
    lastUpdatedAt,
  });
  res.json(successResponse({ data: { _id, resourceUrl: getResourceUrl(req, _id) } }));
}

module.exports = uploadResource;
