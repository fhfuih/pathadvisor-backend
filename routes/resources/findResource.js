'use strict';

const resources = require('../../models/resources');
const ObjectId = require('../../utils/ObjectId');
const NotFoundError = require('../../errors/NotFoundError');

async function findResource(req, res) {
  const { id } = req.params;

  const query = { _id: new ObjectId(id) };
  const data = await resources.findOne(query);

  if (!data) {
    throw new NotFoundError(query, 'Resource not found');
  }
  if (!data.data || !data.data.buffer) {
    throw new NotFoundError(query, 'Resource buffer not found in database');
  }

  res.set('Content-Type', 'application/octet-stream');
  res.send(data.data.buffer);
}

module.exports = findResource;
