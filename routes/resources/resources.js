'use strict';

const bodyParser = require('body-parser');
const router = require('express-promise-router')();
const findResource = require('./findResource');
const uploadResource = require('./uploadResource');
const checkPermissions = require('../../middlewares/auth');
const permissions = require('../../auth/permissions');

router.get('/resources/:id', findResource);
router.post(
  '/resources',
  checkPermissions([permissions['RESOURCE:INSERT']]),
  bodyParser.raw({ limit: '4mb' }),
  uploadResource,
);

module.exports = router;
