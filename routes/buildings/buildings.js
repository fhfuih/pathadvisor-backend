'use strict';

const router = require('express-promise-router')();
const findBuildings = require('./findBuildings');
const upsertBuilding = require('./upsertBuilding');
const deleteBuilding = require('./deleteBuilding');
const checkPermissions = require('../../middlewares/auth');
const permissions = require('../../auth/permissions');

router.get('/buildings', findBuildings);
router.post(
  '/buildings/:id',
  checkPermissions([permissions['BUILDING:INSERT'], permissions['BUILDING:UPDATE']]),
  upsertBuilding,
);
router.delete('/buildings/:id', checkPermissions([permissions['BUILDING:DELETE']]), deleteBuilding);

module.exports = router;
