'use strict';

const successResponse = require('../../responses/successResponse');
const buildings = require('../../models/buildings');

async function findBuildings(req, res) {
  const data = await (await buildings.find()).toArray();
  res.json(successResponse({ data, meta: { count: data.length } }));
}

module.exports = findBuildings;
