'use strict';

const Ajv = require('ajv');

const validator = new Ajv({ allErrors: true });
const mapTiles = require('../../models/mapTiles');
const NotFoundError = require('../../errors/NotFoundError');
const ValidationError = require('../../errors/ValidationError');
const emptyImage = require('../mapImages/emptyImage');

async function findMapTile(req, res) {
  let { x, y, zoomLevel } = req.query;
  const { floorId } = req.params;

  [x, y, zoomLevel] = [x, y, zoomLevel].map(v => parseInt(v, 10));

  const querySchema = {
    definitions: {},
    $schema: 'http://json-schema.org/draft-07/schema#',
    type: 'object',
    additionalProperties: false,
    required: ['floorId', 'x', 'y', 'zoomLevel'],
    properties: {
      floorId: {
        $id: '#/properties/floorId',
        type: 'string',
        minLength: 1,
      },
      x: {
        $id: '#/properties/x',
        type: 'integer',
      },
      y: {
        $id: '#/properties/y',
        type: 'integer',
      },
      zoomLevel: {
        $id: '#/properties/zoomLevel',
        type: 'integer',
      },
    },
  };

  const validate = validator.compile(querySchema);

  if (!validate({ floorId, x, y, zoomLevel })) {
    throw new ValidationError(validate.errors, 'Query string validation errors');
  }

  const query = { floorId, x, y, zoomLevel };
  const [data] = await (await mapTiles.find(query, {
    limit: 1,
    sort: { lastUpdatedAt: -1 },
  })).toArray();

  if (!data) {
    res.set('Content-Type', 'image/png');
    res.status(404).send(await emptyImage());
    return;
  }

  if (!data.data || !data.data.buffer) {
    throw new NotFoundError(query, 'Image buffer not found in database');
  }

  res.set('Content-Type', 'image/png');
  res.send(data.data.buffer);
}

module.exports = findMapTile;
