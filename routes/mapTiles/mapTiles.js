'use strict';

const router = require('express-promise-router')();
const findMapTile = require('./findMapTile');

router.get('/floors/:floorId/map-tiles', findMapTile);

module.exports = router;
