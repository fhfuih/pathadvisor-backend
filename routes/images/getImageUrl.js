'use strict';

function getImageUrl(req, id) {
  const { API_PREFIX = `${req.protocol}://${req.get('host')}${req.baseUrl}` } = process.env;
  return `${API_PREFIX}/images/${id}`;
}

module.exports = getImageUrl;
