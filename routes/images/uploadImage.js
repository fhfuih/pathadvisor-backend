'use strict';

const { Binary } = require('mongodb');
const images = require('../../models/images');
const successResponse = require('../../responses/successResponse');
const getImageUrl = require('./getImageUrl');
const ValidationError = require('../../errors/ValidationError');

async function uploadImage(req, res) {
  const imageBuffer = req.body;

  if (!(imageBuffer instanceof Buffer)) {
    throw new ValidationError(
      null,
      'No request body received. Make sure you set content-type to application/octet-stream',
    );
  }

  const lastUpdatedAt = new Date().getTime();

  const { _id } = await images.insertOne({
    data: new Binary(imageBuffer),
    lastUpdatedAt,
  });
  res.json(successResponse({ data: { _id, imageUrl: getImageUrl(req, _id) } }));
}

module.exports = uploadImage;
