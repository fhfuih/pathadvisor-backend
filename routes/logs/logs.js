'use strict';

const router = require('express-promise-router')();
const checkPermissions = require('../../middlewares/auth');
const permissions = require('../../auth/permissions');

const insertLog = require('./insertLog');
const findLogs = require('./findLogs');

router.post('/logs', insertLog);
router.get('/logs', checkPermissions([permissions['LOG:LIST']], 'session'), findLogs);

module.exports = router;
