'use strict';

const Ajv = require('ajv');

const successResponse = require('../../responses/successResponse');
const suggestions = require('../../models/suggestions');
const convertLiteralToBoolean = require('../../utils/convertLiteralToBoolean');
const ValidationError = require('../../errors/ValidationError');

const validator = new Ajv({ allErrors: true });
async function findSuggestion(req, res) {
  const querySchema = {
    definitions: {},
    $schema: 'http://json-schema.org/draft-07/schema#',
    type: 'object',
    additionalProperties: false,
    properties: {
      resolved: {
        $id: '#/properties/resolved',
        type: 'string',
        enum: ['true', 'false'],
      },
    },
  };
  const validate = validator.compile(querySchema);

  if (!validate(req.query)) {
    throw new ValidationError(validate.errors, 'Query string validation errors');
  }

  const { resolved } = req.query;
  const query = {};
  if (resolved) {
    query.resolved = convertLiteralToBoolean(resolved);
  }

  const data = await (await suggestions.find(query, { sort: { createdAt: -1 } })).toArray();
  res.json(successResponse({ data, meta: { count: data.length } }));
}

module.exports = findSuggestion;
