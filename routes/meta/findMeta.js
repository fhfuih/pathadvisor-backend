'use strict';

const successResponse = require('../../responses/successResponse');
const errorResponse = require('../../responses/errorResponse');
const meta = require('../../models/meta');

async function findMeta(req, res) {
  const data = await meta.findOne({}, { sort: { $natural: -1 } });

  if (!data) {
    res.status(404).json(errorResponse({ message: 'No meta found' }));
    return;
  }

  res.json(successResponse({ data }));
}

module.exports = findMeta;
