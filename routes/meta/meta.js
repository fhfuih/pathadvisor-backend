'use strict';

const router = require('express-promise-router')();
const checkPermissions = require('../../middlewares/auth');
const permissions = require('../../auth/permissions');
const findMeta = require('./findMeta');
const insertMeta = require('./insertMeta');

router.get('/meta', findMeta);
router.post('/meta', checkPermissions([permissions['META:INSERT']]), insertMeta);

module.exports = router;
