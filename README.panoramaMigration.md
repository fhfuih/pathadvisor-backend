# Panorama & Street View Feature Migration

These are some scripts that adds additional data *to the already existing database* required by the new street view functionality. It assumes the database contains all normal data (`nodes` collection especially).

## TOC

[TOC]

## File structure & requirements

Note that the internal file structure is slightly different from `scripts/migration`. The minimum requirement for the scripts to run is:

```
scripts/panoramaMigration
│
│   (scripts...) # included
│
└───raw
│   │   floor.js # included
│   ├───map_data
│   └───map_panorama
│
└───log # included
    └───(scripts...)
```

Where `floor.js` is of the same format as the `migrate` version, except that `NAB` is replaced by `LKS`. (Why can't anyone just type `LSK` correctly? 🦄🦄🦄) `map_data` and `map_panorama` are of the same inner file structures as the legacy backend.

Remember you can leverage Symbolic Links to avoid copying/moving a huge number of files.

## Run

Make sure a MongoDB instance is running. You can refer to [instructions here](README.database.md).

Make sure raw data files (`map_data` & `map_panorama` folders) are placed correctly. A copy can be found in the database dump link.

Then run these scripts *from project root* in the following order
```shell
# do NOT change order
# shell cwd is project root
node scripts/panoramaMigration/nodesAndPanoImages.js
node scripts/panoramaMigration/panoEdges.js
```

## Conflicts & logs

### NodesAndPanoImages

The street view system heavily depends on the existing nodes & edges system. The system adds a new `panoImages` collection storing images, and a new field `panoImage` to the pre-existing `nodes` collection indicating the ID of the `panoImage` document, if the node has one. 

Since the new database indexes nodes in different names, I have to find nodes by their coordinates, and append corresponding `panoImage` references. But some nodes are removed due to the change of the map graph in the 6 years. 

Therefore, after running `nodesAndPanoImages.js`, a detailed log concerning all such nodes can be found in `<root>/scripts/panoramaMigration/log/nodesAndPanoImages.warn.log`. Each line is a valid JSON object containing `message`, (log) `level` and all useful data (ids, names, etc.) for this specific missing node.

In addition, there *may* be other warnings & errors in the log, but normally above is the only case one may encounter.

* warn `[Skip] No node found at specified coordinates` (*very* likely, elaborated above)
* warn `[Skip] More than one nodes found at specified coordinates` (should be impossible)
* error `[Error] MongoDB error on inserting panoImage ${panoImageName} on ${floorId}` (not likely)
* error `[Error] MongoDB error on updating node with panoImage` (not likely)
* error `[Error] Uncaught error while migrating floor ${floorId}` (not likely)

### PanoEdges

`panoEdges` is a new collection that connects nodes with panorama only. These edges will not be used in finding paths, etc., but only when moving around the map in street view mode. The situation is the same as above, except that there mayby missing `fromNode` or `toNode` or both.

* warn `[Skip] [no fromNode] [no toNode] found of panoEdge ${panoEdgeName} on floor ${floorId}` (*very* likely, elaborated above)
* warn `[Skip] [multiple fromNode] [multiple toNode] found of panoEdge ${panoEdgeName} on floor ${floorId}` (should be impossible)
* error `[Error] MongoDB error on inserting panoEdge ${panoEdgeName} on floor ${floorId}` (not likely)
* error `[Error] Uncaught error while migrating floor ${floorId}` (not likely)

## Database Schema

See `models/`. `nodes` is modified and `panoEdges`, `panoImages` are newly added.

Upon reading `map_panorama`, binary image contents are all compressed to 80% JPEG quality for better transmission performance (compressions are done *after* reading "last modified date" meta and are *not* saved to the original files, so "lastUpdatedAt" is still some timestamp in 2012/2013). Compression is togglable in `nodesAndPanoImages.js` line ~89 

```js
panoImageIdByName[panoImage.name] = (await savePanoImage(panoImage, true))._id;
// remove `, true` or set to false to disable compression
```
