'use strict';

const createModel = require('./base/createModel');

const schema = {
  $schema: 'http://json-schema.org/draft-07/schema#',
  type: 'object',
  additionalProperties: false,
  required: ['_id', 'name'],
  properties: {
    _id: {
      $id: '#/properties/_id',
      type: 'string',
      minLength: 1,
    },
    name: {
      $id: '#/properties/name',
      type: 'string',
      minLength: 1,
    },
  },
};

const COLLECTION_NAME = 'buildings';

module.exports = createModel(COLLECTION_NAME, { schema });
