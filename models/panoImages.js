'use strict';

const createModel = require('./base/createModel');

const schema = {
  $schema: 'http://json-schema.org/draft-07/schema#',
  type: 'object',
  additionalProperties: false,
  required: ['westX', 'data', 'lastUpdatedAt'],
  properties: {
    name: {
      $id: '#/properties/name',
      type: 'string',
      minLength: 1,
    },
    floorId: {
      $id: '#/properties/floorId',
      type: 'string',
      minLength: 1,
    },
    westX: {
      $id: '#/properties/westX',
      type: 'number',
    },
    doortagCoordinates: {
      $id: '#/properties/doortagCoordinates',
      type: 'array',
      items: {
        $id: '#/properties/doortagCoordinates/items',
        type: 'array',
        minItem: 4,
        maxItems: 4,
        items: {
          $id: '#/properties/doortagCoordinates/items/value',
          type: 'number',
        },
      },
    },
    doortagIds: {
      $id: '#/properties/doortagIds',
      type: 'array',
      items: {
        $id: '#/properties/doortagIds/items',
        type: 'string',
      },
    },
    data: {
      $id: '#/properties/data',
      type: 'object',
    },
    lastUpdatedAt: {
      $id: '#/properties/lastUpdated',
      type: 'integer',
    },
  },
};

const COLLECTION_NAME = 'panoImages';

module.exports = createModel(COLLECTION_NAME, { schema });
