'use strict';

const createModel = require('./base/createModel');

const schema = {
  definitions: {},
  $schema: 'http://json-schema.org/draft-07/schema#',
  type: 'object',
  additionalProperties: false,
  required: ['ipAddress', 'searchMode', 'from', 'to', 'datetime', 'platform'],
  properties: {
    ipAddress: {
      $id: '#/properties/ipAddress',
      type: 'string',
      format: 'ipv4',
    },
    referer: {
      $id: '#/properties/referer',
      type: 'string',
      minLength: 1,
    },
    userAgent: {
      $id: '#/properties/userAgent',

      type: 'object',
      additionalProperties: false,
      required: ['ua', 'browser', 'engine', 'os', 'device', 'cpu'],
      properties: {
        ua: {
          $id: '#/properties/userAgent/ua',
          type: 'string',
        },
        browser: {
          $id: '#/properties/userAgent/browser',
          type: 'object',
          additionalProperties: false,
          properties: {
            name: {
              $id: '#/properties/userAgent/browser/name',
              type: 'string',
              minLength: 1,
            },
            version: {
              $id: '#/properties/userAgent/browser/version',
              type: 'string',
              minLength: 1,
            },
            major: {
              $id: '#/properties/userAgent/browser/major',
              type: 'string',
              minLength: 1,
            },
          },
        },
        engine: {
          $id: '#/properties/userAgent/engine',
          type: 'object',
          additionalProperties: false,
          properties: {
            name: {
              $id: '#/properties/userAgent/engine/name',
              type: 'string',
              minLength: 1,
            },
            version: {
              $id: '#/properties/userAgent/engine/version',
              type: 'string',
              minLength: 1,
            },
          },
        },
        os: {
          $id: '#/properties/userAgent/os',
          type: 'object',
          additionalProperties: false,
          properties: {
            name: {
              $id: '#/properties/userAgent/os/name',
              type: 'string',
              minLength: 1,
            },
            version: {
              $id: '#/properties/userAgent/os/version',
              type: 'string',
              minLength: 1,
            },
          },
        },
        device: {
          $id: '#/properties/userAgent/device',
          type: 'object',
          additionalProperties: false,
          properties: {
            vendor: {
              $id: '#/properties/userAgent/device/vendor',
              type: 'string',
              minLength: 1,
            },
            model: {
              $id: '#/properties/userAgent/device/model',
              type: 'string',
              minLength: 1,
            },
            type: {
              $id: '#/properties/userAgent/device/type',
              type: 'string',
              minLength: 1,
            },
          },
        },
        cpu: {
          $id: '#/properties/userAgent/cpu',
          type: 'object',
          additionalProperties: false,
          properties: {
            architecture: {
              $id: '#/properties/userAgent/cpu/architecture',
              type: 'string',
              minLength: 1,
            },
          },
        },
      },
    },
    searchMode: {
      $id: '#/properties/searchMode',
      type: 'string',
      enum: ['normal', 'nearest'],
    },
    from: {
      $id: '#/properties/from',
      type: 'string',
      minLength: 1,
    },
    to: {
      $id: '#/properties/to',
      type: 'string',
      minLength: 1,
    },
    datetime: {
      $id: '#/properties/datetime',
      type: 'integer',
    },
    platform: {
      $id: '#/properties/platform',
      type: 'string',
      enum: ['desktop', 'mobile'],
    },
  },
};

const COLLECTION_NAME = 'logs';

module.exports = createModel(COLLECTION_NAME, { schema });
