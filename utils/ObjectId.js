'use strict';

const mongodb = require('mongodb');
const ValidationError = require('../errors/ValidationError');

function ObjectId(id) {
  let _id;

  try {
    _id = mongodb.ObjectId(id);
  } catch (err) {
    throw new ValidationError(null, 'id passed must be a string of 24 hex characters');
  }

  return _id;
}

module.exports = ObjectId;
