'use strict';

const winston = require('winston');
const AuthError = require('../errors/AuthError');
const NotFoundError = require('../errors/NotFoundError');
const ValidationError = require('../errors/ValidationError');

// https://github.com/winstonjs/winston/issues/1243#issuecomment-423558375

function prodFormat() {
  const replaceError = ({ label, level, message, stack, reasonObject, query, ip }) => ({
    label,
    level,
    message,
    stack,
    reasonObject,
    query,
    ip,
  });
  const replacer = (key, value) => (value instanceof Error ? replaceError(value) : value);
  return winston.format.json({ replacer });
}

function devFormat() {
  const formatMessage = info => `${info.level} ${info.message}`;
  const formatError = info => {
    let message = `${info.level} ${info.message}`;

    if (info instanceof AuthError) {
      message += ` - ${info.reasonObject.reason}`;
      message += info.reasonObject.username ? ` for ${info.reasonObject.username}` : '';
      return message;
    }

    if (info instanceof NotFoundError) {
      message += `- query: ${JSON.stringify(info.query, null, 2)}`;
      return message;
    }

    if (info instanceof ValidationError) {
      return message;
    }

    message += `\n${info.stack}`;

    return message;
  };
  const format = info => (info instanceof Error ? formatError(info) : formatMessage(info));
  return winston.format.combine(winston.format.colorize(), winston.format.printf(format));
}

const logger = winston.createLogger({
  level: 'info',
  format: process.env.NODE_ENV === 'production' ? prodFormat() : devFormat(),
  transports: [new winston.transports.Console()],
});

module.exports = logger;
