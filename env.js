'use strict';

const dotenv = require('dotenv');

dotenv.config();

const NODE_ENV = process.env.NODE_ENV;
if (!NODE_ENV) {
  throw new Error('The NODE_ENV environment variable is required but was not specified.');
}

module.exports = function loadEnv() {};
