'use strict';

class ValidationError extends Error {
  constructor(validationErrors, ...params) {
    super(...params);

    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, ValidationError);
    }

    this.validationErrors = validationErrors;
  }
}

module.exports = ValidationError;
