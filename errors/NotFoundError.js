'use strict';

class NotFoundError extends Error {
  constructor(query, ...params) {
    super(...params);

    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, NotFoundError);
    }

    this.query = query;
  }
}

module.exports = NotFoundError;
