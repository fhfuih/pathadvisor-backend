'use strict';

const getClient = require('./getClient');

module.exports = async function getCollection(collectionName) {
  const db = (await getClient()).db();
  return db.collection(collectionName);
};
